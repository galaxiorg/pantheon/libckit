--[[
libckit | Pantheon Compatibility Kit
        | Pantheon Project (galaxiorg/pantheon)

Filename
  modules/include.lua
Author
  daelvn (https://gitlab.com/daelvn)
Version
  libckit-0.1
License
  MIT License (LICENSE.md)
Platforms
  CraftOS
  CraftOS-based
Description
  ifind() iterator implementaion from Pantheon. Iterates over occurences of a
  pattern in a string, giving the start and end position.
]]--

local function ifind(str, pat)
	local i  = 0
	local le = 0
	return function()
		i = i + 1
		local s, e = str:find(pat, le)
		if e then le = e; return i,s,e end
	end
end

return ifind, "ifind"
