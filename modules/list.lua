--[[
libckit | Pantheon Compatibility Kit
        | Pantheon Project (galaxiorg/pantheon)

Filename
  modules/list.lua
Author
  daelvn (https://gitlab.com/daelvn)
Version
  libckit-0.1
License
  MIT License (LICENSE.md)
Platforms
  CraftOS
  CraftOS-based
Description
  Iterator function that only uses values and not keys
]]--

local function list(t)
   local index, count = 0, #t
   return function ()
      index = index + 1
      if index <= count
      then
         return t[index]
      end
   end
end

return list, "list"
