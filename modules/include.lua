--[[
libckit | Pantheon Compatibility Kit
        | Pantheon Project (galaxiorg/pantheon)

Filename
  modules/include.lua
Author
  daelvn (https://gitlab.com/daelvn)
Version
  libckit-0.2
License
  MIT License (LICENSE.md)
Platforms
  CraftOS
  CraftOS-based
Description
  Implementation of the include() function from Pantheon
]]--

-- Load functions
local lambda  = function(f,...) return f(table.unpack(arg)) end

-- Declare function
local function include(library, ...)
  -- Needed variables
  local argn, argl = lambda(
    function(...)
      local n = arg.n; arg.n = nil
      return n,arg
    end, ...
  )
  local path = library:gsub(".","/")
  local prefixes = {
    "/rom/apis/",
    "/apis/",
    "/lib/",
    "/", ""
  }
  -- Calculate possible paths
  local paths = {}
  for _, prefix in ipairs(prefixes) do
    table.insert(paths, prefix..path)
    table.insert(paths, prefix..path..".lua")
    table.insert(paths, prefix..path..".min.lua")
    table.insert(paths, prefix..path.."/main.lua")
    table.insert(paths, prefix..path.."/main.min.lua")
  end
  -- Load the file that exists
  local export = {}
  for _, path in ipairs(paths) do
    if fs.exists(path) and (not fs.isDir(path)) then
      export = loadfile(path)(table.unpack(arg))
    end
  end
  -- Return the latest load or error if it wasn't found
  if export == {} then error("libckit: include.lua: Could not find source file") end
  return export
end

-- Return the function
return include, "include"
