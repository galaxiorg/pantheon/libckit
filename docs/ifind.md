# libckit Documentation
ifind.lua - Find pattern iterator
## Using the function
You can use this as a normal iterator in a `for` loop. You pass a string and a pattern to it and it will return the occurence number, start index and end index.
This would be a basic usage example:
```lua
for n,st,en in ifind("A! basic! string!", ".!")
  print("Number: "..tostring(n))
  print("Start index: "..tostring(st))
  print("End index: "..tostring(en))
  print("Substring: "..("A! basic! string!"):sub(st,en))7
  print("")
end
--[[
Prints:
  Number: 1
  Start index: 1
  End index: 2
  Substring: A!

  Number: 2
  Start index: 8
  End index: 9
  Substring: c!

  Number: 3
  Start index: 16
  End index: 17
  Substring: g!
]]--
```
