# libckit Documentation
include.lua - Find and load libraries
## Using the function
You pass a resource locator (Example `lib.module.api`. Basically a relative path with dots instead of slashes.) and it will search in various directories (`local prefixes` in the source code), and in those directories.

`lib.module.api`, by default, will search in:
```text
Absolute paths:
  /rom/apis/lib/module/api
  /rom/apis/lib/module/api.lua
  /rom/apis/lib/module/api.min.lua
  /rom/apis/lib/module/api/main.lua
  /rom/apis/lib/module/api/main.min.lua
  /apis/lib/module/api
  /apis/lib/module/api.lua
  /apis/lib/module/api.min.lua
  /apis/lib/module/api/main.lua
  /apis/lib/module/api/main.min.lua
  /lib/lib/module/api
  /lib/lib/module/api.lua
  /lib/lib/module/api.min.lua
  /lib/lib/module/api/main.lua
  /lib/lib/module/api/main.min.lua
  /lib/module/api
  /lib/module/api.lua
  /lib/module/api.min.lua
  /lib/module/api/main.lua
  /lib/module/api/main.min.lua
Relative paths:
  lib/module/api
  lib/module/api.lua
  lib/module/api.min.lua
  lib/module/api/main.lua
  lib/module/api/main.min.lua
```

You can optionally pass more arguments to `include` if you wish to pass arguments to the library when loading.
This would be a basic example usage:
```lua
  local mylib = include "org.mylib"
```
