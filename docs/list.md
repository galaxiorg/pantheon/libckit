# libckit Documentation
list.lua - Table list iterator
## Using the function
A list is defined as an unordered numerical-indexed table, when this tables don't need to be ordered, you can iterate them without the key, using `list()`.  
Usage of the function:
```lua
local mylist = {1,2,3,4,5}
for n in list(mylist) do print(n) end
--[[
Prints:
  1
  2
  3
  4
  5
]]--
```
