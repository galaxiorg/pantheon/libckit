--[[
libckit | Pantheon Compatibility Kit
        | Pantheon Project (galaxiorg/pantheon)

Filename
  install.lua
Author
  daelvn (https://gitlab.com/daelvn)
Version
  libckit-0.1
License
  MIT License (LICENSE.md)
Platforms
  CraftOS
  CraftOS-based
Description
  Install script for libckit
]]--

-- Prompt for install paths
io.write("Library install path (/lib/): ")
local ip_lib = io.read()
io.write("Script install path (/bin/):")
local ip_bin = io.read()
io.write("Documentation install path (/opt/):")
local ip_opt = io.read()

if ip_lib == "" then ip_lib = "/lib/" end
if ip_bin == "" then ip_bin = "/bin/" end
if ip_opt == "" then ip_opt = "/opt/" end

-- Create folders
fs.makeDir(ip_lib.."libckit/")
fs.makeDir(ip_opt.."libckit/")
fs.makeDir(ip_opt.."libckit/docs/")

-- Adapt paths
ip_lib = ip_lib .. "libckit/"
ip_opt = ip_opt .. "libckit/"

-- Create code folders
fs.makeDir(ip_lib.."modules/")

-- Start moving
-- ip_lib
fs.move("main.lua", ip_lib.."main.lua")
-- ip_lib/modules
for _, file in pairs(fs.list("modules/")) do
  fs.move("modules/"..file, ip_lib.."modules/"..file)
end
-- ip_bin
fs.move("bin/ckit-init", ip_bin.."ckit-init")
-- ip_opt
fs.move("LICENSE.md", ip_opt.."LICENSE.md")
fs.move("README.md", ip_opt.."README.md")
-- ip_opt/docs
for _, file in pairs(fs.list("docs/")) do
  fs.move("docs/"..file, ip_opt.."docs/"..file)
end

print("libckit was installed onto your system.")
