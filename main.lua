--[[
libckit | Pantheon Compatibility Kit
        | Pantheon Project (galaxiorg/pantheon)

Filename
  main.lua
Author
  daelvn (https://gitlab.com/daelvn)
Version
  libckit-0.1
License
  MIT License (LICENSE.md)
Platforms
  CraftOS
  CraftOS-based
Description
  libckit is a compatibility kit that serves Pantheon functions in other OSes.
]]--

-- Create a basic function for being clean
function table.find(table, value)
  for k,v in pairs(table) do
    if v == value then return true end
  end
  return false
end

local libckit = {}
-- Load elements
function libckit.loadModules(...)
  for _, module in ipairs(fs.list("modules/")) do
    if not table.find(arg) then
      local resource, name = dofile(module)
      if resource then
        _G[name] = resource
      end
    end
  end
end

-- Return libary
return libckit
